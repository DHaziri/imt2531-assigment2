// #include "graphicsFunctions.h"
#include <creature.h>
#include "global.h"


Creature::Creature(char c, Level l, GLuint PID)
{
	creID = 'X';

	lvlH = l.getHeight();
	lvlW = l.getWidth();

	// Create the ghost 3d model path and the variable for texture path
	char * model = "../res/objects/cube.obj";
	char * texture = "../res/textures/HazelnutBark.png";


	if (c == 'M')			// M for pacMan, changes both model and texture paths
	{
		creID = 'M';

		// Creates changable path
		char tmpTXT[50] = "../res/textures/pacman/PM-F0.obj";

		// Fills in the pacman life texture array
		for (int i = 1; i <= 10; i++)	// Make amount of life matches texture by
		{								//	having life 1 matching arrayplace one etc
			// Changes the char which has the number of texture to the right number
			tmpTXT[27] = '0' + i;
			if (i == 10)				// life 10 is 0 to avoid chaning more in the char array
			{
				tmpTXT[27] = '0';
			}
			//pmTexturePath.resize(pmTexturePath.size() + 1);
			//strcpy(pmTexturePath[i], tmpTXT);
			pmTexturePath.push_back(tmpTXT);
		}

		// Fills pacman animation array
		changePacmanTexture(5, PID);

	}
	else {
		if (c == 'B')	// B for Blinky, changes texture path
		{
			creID = 'B';
			texture = "../res/textures/HazelnutBark.png";
		}
		else if (c == 'P')	// P for Pinky, changes texture path
		{
			texture = "../res/textures/HazelnutBark.png";
		}
		else if (c == 'I')	// I for Inky, changes texture path
		{
			texture = "../res/textures/HazelnutBark.png";
		}
		else if (c == 'C')	// C for Clyde, changes texture path
		{
			texture = "../res/textures/HazelnutBark.png";
		}

		// Creates a temp model with right info, and fills in the model with th
		Model_3D tempC(model, texture, PID);
		//creature = tempC;
		creature.push_back(tempC);
	}


	x = l.getXC(creID);
	y = l.getYC(creID);
	if (creID == 'X') 
	{ 
		ifOut = '2';
	}
	else 
	{ 
		ifOut = '0'; 
	}

}


// Fills in Pacman animation array, and changes texture based on life
void Creature::changePacmanTexture(int l, GLuint PID)
{
	// Creates changable path to send  through
	char tmpObjTXT[50] = "../res/objects/pacman/pacman0.obj";
	char * pathObjTXT;
	const char * pathTexTXT = pmTexturePath[l].c_str();

	// Fills inn the pacman Model animation array
	for (int i = 0; i < ANIFRAMES; i++)
	{
		// Changes the char which has the number of model to the right number
		tmpObjTXT[28] = '0' + i;
		pathObjTXT = tmpObjTXT;

		// Creats a temp 3D model and adds it to the actual model
		Model_3D tmp(pathObjTXT, pathTexTXT, PID);
		//pacman[i] = tmp;
		creature.push_back(tmp);
	}

	// Delete the pointers
	pathObjTXT = NULL;
	delete pathObjTXT;
	pathTexTXT = NULL;
	delete pathTexTXT;
}



// returns the Y and X coordinates, used when creature has moved
int Creature::getY() 
{ 
	return y; 
}
int Creature::getX() 
{ 
	return x; 
}

// Returns the direction creature is looking, used when creature has moved
int Creature::getDir()
{
	return dir;
}

char Creature::getChar() { return creID; }
void Creature::changeChar(char c) { creID = c; }


bool Creature::ifPacman(int yp, int xp) 
{
	if (x == xp && y == yp) 
	{
		return true;
	}
	
	return false;
}

void Creature::makeEateble(bool e, bool t) 
{ 
	eateble = e; 
	eaten = t; 


}
bool Creature::isEateble() 
{ 
	return eateble;  
}
bool Creature::noKill() 
{ 
	if (eaten) 
	{ 
		return true; 
	}
	return false;
}


void Creature::draw(const double dt, int f, glm::mat4 proj, glm::mat4 view, std::vector<glm::vec3> lights, std::vector<glm::vec3> lightColor)
{
	/*if (xl == 0) { xl = x; }
	if (yl == 0) { yl = y; }
	if (dirl == 0) { dirl = dir; }

	if (ifCat && eateble) {	drawBox(xl, yl, 6 + animStep, 5); }
	else if (ifCat) { drawBox(xl, yl, 4 + animStep, 4+id); }
	else if (eateble) { drawBox(xl, yl, 6 + animStep, 4); }
	else { drawBox(xl, yl, 4 + id * 2 + animStep, dirl); }

	if (drawTimer > 0) { drawTimer -= dt; }
	else {
		animStep++;
		if (animStep == 2) { animStep = 0; }
		drawTimer = 0.06;
	}
	*/

	float drawX = x, drawY = y;
	if (dir == 0)
	{
		drawY = (1 - dt) * y + (dt) * (y - 1);
	}
	else if (dir == 1)
	{
		drawX = (1 - dt) * x + (dt) * (x + 1);
		
	}
	else if (dir == 2)
	{
		drawY = (1 - dt) * y + (dt) * (y + 1);
	}
	else if (dir == 3)
	{
		drawX = (1 - dt) * x + (dt) * (x - 1);
	}

	if (creID == 'M')
	{
		creature[f].draw(glm::vec3(x, 0, y), proj, view, lights, lightColor);
	}
	else
	{
		creature[0].draw(glm::vec3(x, 0, y), proj, view, lights, lightColor);
	}
	
}



void Creature::move(const double dt, Level level, int cl)
{
	dir = rand() % 4;

	if (moveTimer > 0) {
		moveTimer -= dt;
	}
	else {


		if (eaten) {
			ifOut = 2;

			x = level.getXC('X');
			y = level.getYC('X');
			eaten = false;
		}
		else {

			if ((dir == 1 && level.ifRoadBlock(y - 1, x) == '0') || (dir == 1 && level.ifRoadBlock(y - 1, x) == ifOut))
			{
				if (y == 0) 
				{ 
					y = lvlH - 1; 
				}
				else 
				{ 
					y--; 
				}

			}
			else if ((dir == 2 && level.ifRoadBlock(y, x - 1) == '0') || (dir == 2 && level.ifRoadBlock(y, x - 1) == ifOut))
			{
				if (x == 0) 
				{ 
					x = lvlW - 1; 
				}
				else
				{
					x--;
				}

			}
			else if ((dir == 0 && level.ifRoadBlock(y + 1, x) == '0') || (dir == 0 && level.ifRoadBlock(y + 1, x) == ifOut))
			{
				if (y == lvlH - 1)
				{
					y = 0;
				}
				else
				{
					y++;
				}
			}
			else if ((dir == 3 && level.ifRoadBlock(y, x + 1) == '0') || (dir == 3 && level.ifRoadBlock(y, x + 1) == ifOut))
			{
				if (x == lvlW - 1)
				{
					x = 0;
				}
				else
				{
					x++;
				}
			}

			// If ghosts are on normal 0 ground, then can only walk on zero ground
			if (level.ifRoadBlock(y, x) == 0)
			{
				ifOut = '0';
			}
		}
		moveTimer = BASEANIMOVETIMER - FASTERSPEED * cl;
	}
}

