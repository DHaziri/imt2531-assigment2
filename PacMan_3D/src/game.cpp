#include <game.h>
#include <graphics_functions.h>
#include <sstream>
#include <level.h>
#include <creature.h>
#include <pacman.h>
#include <global.h>
#include <model_2D.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>


Game::Game()
{
    // Initialise GLFW
    if (!glfwInit())
    {
        fprintf(stderr, "Failed to initialize GLFW\n");
        exit(-1);
    }

    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Open a window and create its OpenGL context
    window = glfwCreateWindow(1024, 768, "pacman", NULL, NULL);
    if (window == NULL) {
        fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible.\n");
        glfwTerminate();
        exit(-1);
    }
    glfwMakeContextCurrent(window);

    // Initialize GLEW
    glewExperimental = true; // Needed for core profile
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        glfwTerminate();
        exit(-1);
    }

    // Ensure we can capture the escape key being pressed below
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

    // Dark blue background
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Cull triangles which normal is not towards the camera
	//glEnable(GL_CULL_FACE);
	//glCullFace(GL_FRONT);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);/**/

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

    programID = Graphics::loadShaders("../res/shaders/shader3D.vert", "../res/shaders/shaderCool.frag");
	GUIProgramID = Graphics::loadShaders("../res/shaders/shader2D.vert", "../res/shaders/shader.frag");



	// Createa all the 2D model arrays
	load2DModels("../res/sprites/numbers/0.png", 10, 23, numbers);
	load2DModels("../res/sprites/choices/0StartGame.png", 2, 23, cStartGame);
	load2DModels("../res/sprites/choices/0Highscores.png", 2, 23, cHighscores);
	load2DModels("../res/sprites/choices/0Quit.png", 2, 23, cQuit);
	load2DModels("../res/sprites/choices/0Resume.png", 2, 23, cResume);
	load2DModels("../res/sprites/choices/0Yes.png", 2, 23, cYes);
	load2DModels("../res/sprites/choices/0No.png", 2, 23, cNo);

	// Creates temp 2D models
	Model_2D tmpHSB("../res/sprites/HighscoreBox.png", GUIProgramID);
	Model_2D tmpMB("../res/sprites/MenuBox.png", GUIProgramID);

	Model_2D tmpTPM("../res/sprites/titles/tPacman.png", GUIProgramID);
	Model_2D tmpTHS("../res/sprites/titles/tHighscores.png", GUIProgramID);
	Model_2D tmpTP("../res/sprites/titles/tPause.png", GUIProgramID);
	Model_2D tmpTGM("../res/sprites/titles/tGameOver.png", GUIProgramID);
	Model_2D tmpTTA("../res/sprites/titles/tTryAgain.png", GUIProgramID);

	// To fill the actual 2D models
	higscoreBox = tmpHSB;
	menuBox = tmpMB;

	tPacMan = tmpTPM;
	tHighscores = tmpTHS;
	tPause = tmpTP;
	tGameOver = tmpTGM;
	tTryAgain = tmpTTA;

		
	// Positions the 2D_models that dont move
	higscoreBox.make_square(glm::vec2(0.65, -0.85), 0.30);
	menuBox.make_square(glm::vec2(0, 0), 1);

	tPacMan.make_square(glm::vec2(0, 0.6), 1);
	tHighscores.make_square(glm::vec2(0, 0.8), 0.7);
	tPause.make_square(glm::vec2(0, 0.5), 1);
	tGameOver.make_square(glm::vec2(0, 0.37), 1);
	tTryAgain.make_square(glm::vec2(0, -0.27), 0.9);

	m_sLoop2DModels(cStartGame, 0.15);
	m_sLoop2DModels(cHighscores, -0.2);
	m_sLoop2DModels(cResume, 0.07);
	m_sLoop2DModels(cYes, -0.6, -0.35);
	m_sLoop2DModels(cNo, -0.6, 0.35);
}



// The main game, acts as a new int main()
void Game::start() 
{
	// Finds neccesary files to build the level
	std::ifstream readLevel("../res/level_data/level.txt");
	std::ifstream readPoints("../res/level_data/points.txt");

	// If any or both of the files doesn't exist
	if (!readLevel || !readPoints) 
	{
		std::cout << "Game cannot open. Files are missing.";
		exit(-1);
	}

	// Inizializes the level, points and creatures
	Level tempLevel(readLevel, readPoints, programID, GUIProgramID);
	level = tempLevel;
	level.countPellets();
	creatureRestart();
	initializeHS();
	


    // Projection matrix : 45� Field of View, 16:9 ratio, display range : 0.1 unit <-> 100 units
    Projection = glm::perspective(glm::radians(45.0f), 16.0f / 9.0f, 0.1f, 100.0f);

    // Camera matrix
    View = glm::lookAt(
        glm::vec3(13, 20, 16), // Camera is at (0,3,6), in World Space
        glm::vec3(14, 0, 15), // and looks at the origin
        glm::vec3(0, 0, -1)  // Head is up (set to 0,-1,0 to look upside-down)
    );


	auto time_last_frame = glfwGetTime();
	

    do 
	{
        // Clear the screen.
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Compute delta time 
		const auto dt = glfwGetTime() - time_last_frame;
		time_last_frame = glfwGetTime();


		// Updates Mechanics, then draws
		mechanics(dt);	
		drawGame(dt);
				

        // Swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();

    } // Check if the ESC key was pressed or the window was closed
    while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
        glfwWindowShouldClose(window) == 0);


    // Close OpenGL window and terminate GLFW
    glfwTerminate();
}








// Handles the controls of the game
void Game::controls(GLFWwindow * window, int key, int scancode, int action, int mods)
{
	// Main Menu
	if (gameMode == MAINMENU) 
	{

		if (key == GLFW_KEY_W && action == GLFW_PRESS)
		{
			optionSelected--;		
			if (optionSelected < 0) // Makes sure you can scroll forever
			{ 
				optionSelected = 2; 
			}
		}
		else if (key == GLFW_KEY_S && action == GLFW_PRESS)
		{
			optionSelected++;		
			if (optionSelected > 2) // Makes sure you can scroll forever
			{ 
				optionSelected = 0; 
			}
		}
		else if (key == GLFW_KEY_ENTER && action == GLFW_PRESS)
		{
			if (optionSelected == 0) 
			{ 
				gameMode = GAME; 
			}
			else if (optionSelected == 1) 
			{ 
				gameMode = HS; 
			}
			else if (optionSelected == 2) 
			{ 
				exit(-1);
			}
			optionSelected = 0;		// Resets in case another function might use it
		}
	}
	
	// Game, The actual pacman game
	if (gameMode == GAME) 
	{
		// Pacmans controls
		if (key == GLFW_KEY_W && action == GLFW_PRESS)
		{
			pacman.changeNextDir(0);
		}
		else if (key == GLFW_KEY_S && action == GLFW_PRESS)
		{
			pacman.changeNextDir(2);
		}
		else if (key == GLFW_KEY_A && action == GLFW_PRESS)
		{
			pacman.changeNextDir(-1);
		}
		else if (key == GLFW_KEY_D && action == GLFW_PRESS)
		{
			pacman.changeNextDir(1);
		}
		else if ((key == GLFW_KEY_P && action == GLFW_PRESS) || (key == GLFW_KEY_BACKSPACE && action == GLFW_PRESS))
		{
			gameMode = PAUSE;
			optionSelected == 0;
		}
	}

	// Pause, pause menu
	else if (gameMode == PAUSE)
	{
		if (key == GLFW_KEY_W && action == GLFW_PRESS)
		{
			optionSelected--;	
			if (optionSelected < 0) // Makes sure you can scroll forever
			{ 
				optionSelected = 1; 
			}
		}
		else if (key == GLFW_KEY_S && action == GLFW_PRESS)
		{
			optionSelected++;	
			if (optionSelected > 1) // Makes sure you can scroll forever
			{ 
				optionSelected = 0; 
			}
		}
		else if (key == GLFW_KEY_ENTER && action == GLFW_PRESS)
		{
			if (optionSelected == 0) 
			{ 
				gameMode = GAME; 
			}
			else if (optionSelected == 1) 
			{ 
				gameMode = MAINMENU;
				gameReset = FULL;
			}
			optionSelected = 0;		// Resets incase another function might use it
		}
	}

	// Game Over
	else if (gameMode == GAMEOVER) 
	{
		if (key == GLFW_KEY_A && action == GLFW_PRESS)
		{
			optionSelected--;
			if (optionSelected < 0) // Makes sure you can scroll forever
			{
				optionSelected = 1;
			}
		}
		else if (key == GLFW_KEY_D && action == GLFW_PRESS)
		{
			optionSelected++;
			if (optionSelected > 1) // Makes sure you can scroll forever
			{
				optionSelected = 0;
			}
		}
		else if (key == GLFW_KEY_ENTER && action == GLFW_PRESS)
		{
			if (optionSelected == 0) 
			{ 
				gameMode = GAME; 
			}
			else if (optionSelected == 1) 
			{ 
				gameMode = MAINMENU;
			}
			optionSelected = 0;	// Resets in case another function might use it
		}
	}


	// See Highscores, can only go back
	else if (gameMode == HS) 
	{
		if (key == GLFW_KEY_BACKSPACE && action == GLFW_PRESS)
		{
			gameMode = MAINMENU;
		}
	}
}



// Handels the mechanics of the game
void Game::mechanics(const double dt)
{
	// No Main Menu as it as no mechanics

	// Start Game, The actual pacman game 
	if (gameMode == GAME) 
	{
		// Checks if game needs to be reset
		if (gameReset == FULL) 
		{ 
			fullRestart();
			completedLevels = 0;
		}
		else if (gameReset == NEWLEVEL) 
		{ 
			fullRestart(pacman.compareScore(), pacman.getLife());
			completedLevels++;
		}
		else if (gameReset == LOSTLIFE) 
		{ 
			creatureRestart(pacman.compareScore(), pacman.getLife()); 
		}
		gameReset = NORESET;



		// Move the creatures
		pacman.move(dt, level, completedLevels);
		inky.move(dt, level, completedLevels);
		pinky.move(dt, level, completedLevels);
		blinky.move(dt, level, completedLevels);
		clyde.move(dt, level, completedLevels);



		// Gets pacmans cordanates to avoid getting it from the functions 100000 times
		int py = pacman.getY(); 
		int px = pacman.getX();

		// Checks if Pacman meets the ghosts, if ghost and eateble = poitns, if not eateble, lose a life
		if (blinky.ifPacman(py, px) && blinky.isEateble())
		{
			pacman.hasEaten(true, programID);			// Adds to counter and to points
			blinky.makeEateble(false, true);// No loger eatable, but now eaten so it can respawn
		}
		else if (!blinky.noKill() && blinky.ifPacman(py, px))
		{
			pacman.changeLife(-1, programID);
			gameReset = LOSTLIFE;
		}

		if (inky.ifPacman(py, px) && inky.isEateble())
		{
			pacman.hasEaten(true, programID);			// Adds to counter and to points
			inky.makeEateble(false, true);	// No loger eatable, but now eaten so it can respawn
		}
		else if (!inky.noKill() && inky.ifPacman(py, px))
		{
			pacman.changeLife(-1, programID);
			gameReset = LOSTLIFE;
		}

		if (pinky.ifPacman(py, px) && pinky.isEateble())
		{
			pacman.hasEaten(true, programID);			// Adds to counter and to points
			pinky.makeEateble(false, true); // No loger eatable, but now eaten so it can respawn
		}
		else if (!pinky.noKill() && pinky.ifPacman(py, px))
		{
			pacman.changeLife(-1, programID); 
			gameReset = LOSTLIFE;
		}

		if (clyde.ifPacman(py, px) && clyde.isEateble())
		{
			pacman.hasEaten(true, programID);			// Adds to counter and to points
			clyde.makeEateble(false, true); // No loger eatable, but now eaten so it can respawn
		}
		else if (!clyde.noKill() && clyde.ifPacman(py, px))
		{
			pacman.changeLife(-1, programID);
			gameReset = LOSTLIFE;
		}

		// Times that the ghost turns back to normal after a certian amount of time
		if (blinky.isEateble() || inky.isEateble() || pinky.isEateble() || clyde.isEateble()) 
		{
			if (safeGhostTimer > 0)
			{
				safeGhostTimer -= dt;
			}
			else 
			{
				blinky.makeEateble(false);
				inky.makeEateble(false);
				pinky.makeEateble(false);
				clyde.makeEateble(false);
				pacman.hasEaten(false, programID);
				dayLight = false;
				safeGhostTimer = MAXGHOSTTIME;
			}
		}



		// Checks for fruit and pellets
		level.ifFruit(dt);
		int ifPoints = level.checkPellets(py, px);
		pacman.addHS(ifPoints); // Add points to highscore
		if (ifPoints == 50)		// If points is 50(a big pellet), make the ghosts eateble
		{
			blinky.makeEateble(true);
			inky.makeEateble(true);
			pinky.makeEateble(true);
			clyde.makeEateble(true);
			dayLight = true;
			safeGhostTimer = MAXGHOSTTIME;
		}



		// Goes to next animation frame
		aniFrames++;
		if (aniFrames >= ANIFRAMES)
		{
			aniFrames = 0;
		}



		// If no pellet left =  New Level
		if (level.howManyPellets() == 0)
		{
			gameReset = NEWLEVEL;
		}

		// No life left = Game Over
		if (pacman.getLife() == 0)
		{
			gameMode = GAMEOVER;
			saveHS = true;		// Make  saveHS true to make sure highscore is saved only once
		}

		updateCamera(dt);
	}


	// No Pause as it has no mechanics


	// Game over
	else if (gameMode == GAMEOVER) 
	{		
		if (saveHS == true) // Saves new Highscore
		{
			saveHS = newHighscore();
		}
		gameReset = FULL;	// Resets before next game
	}


	// No Highscore as it has no mechanics
}



// Draws out the grafics
void Game::drawGame(const double dt)
{

	if (gameMode == MAINMENU)
	{					 
		m_sLoop2DModels(cQuit, -0.55);	// Declared here as the sprites are used two places or more

		int s = 1, h = 0, q = 0;		// Asummes that user has StartGame(s) selected
		if (optionSelected == 1)		// If Highscores(h) is selected, switch
		{ 
			s = 0;
			h = 1;
		}
		else if (optionSelected == 2)	// If Quit(q) is selected, switch
		{
			s = 0;
			q = 1;
		}

		tPacMan.draw();
		cStartGame[s].draw();
		cHighscores[h].draw();
		cQuit[q].draw();
	}

	// Draws the level both when playing the game and pausing
	else if (gameMode == GAME || gameMode == PAUSE)
	{
		level.draw(dt, aniFrames, Projection, View, getLights(), getLightColors());

		pacman.draw(dt, aniFrames, Projection, View, getLights(), getLightColors());
		blinky.draw(dt, aniFrames, Projection, View, getLights(), getLightColors());
		pinky.draw(dt, aniFrames, Projection, View, getLights(), getLightColors());
		inky.draw(dt, aniFrames, Projection, View, getLights(), getLightColors());
		clyde.draw(dt, aniFrames, Projection, View, getLights(), getLightColors());



		if (gameMode == GAME)		// Draws highscore and maps only when playing
		{
			drawHS(pacman.compareScore(), 0.51, -0.825, 0.06, 0.03);
			higscoreBox.draw();
		}
		else if (gameMode == PAUSE)	// Draws pause box only when pausing
		{
			int r = 1, q = 0;		// Asummes that user has Resume(r) selected
			if (optionSelected == 1)// If Quit(q) is selected, switch
			{
				r = 0;
				q = 1;
			}
									// Declared here as the sprites are used two places or more
			m_sLoop2DModels(cQuit, -0.33);

			tPause.draw();
			cResume[r].draw();
			cQuit[q].draw();
			menuBox.draw();
		}
	}

	// Draws Game Over
	else if (gameMode == GAMEOVER)
	{
		int y = 1, n = 0;		// Asummes that user has Yes(y) selected
		if (optionSelected == 1)// If No(n) is selected, switch
		{
			y = 0;
			n = 1;
		}

		tGameOver.draw();
		drawHS(pacman.compareScore(), -0.25, 0.09, 0.1, 0.06);
		tTryAgain.draw();
		cYes[y].draw();
		cNo[n].draw();
	}

	// Draws Highscores
	else if (gameMode == HS)
	{
		tHighscores.draw();
		for (int i = 0; i < 10; i++)	// Draws each saved highscore
		{
			drawHS(highscoreList[i], -0.179, 0.6-(0.16*i), 0.072, 0.045);
		}

	} 
}


// Updates camera in relation to Pacman
void Game::updateCamera(float dt)
{
	float pacX = pacman.getX();
	float pacY = pacman.getY();
	glm::vec3 camPos;
	if (pacman.getDir() == 0)
	{
		camPos = glm::vec3(pacX, 0.75, pacY +1.5);
	}
	else if (pacman.getDir() == 1)
	{
		camPos = glm::vec3(pacX -1.5, 0.75, pacY);
	}
	else if (pacman.getDir() == 2)
	{
		camPos = glm::vec3(pacX, 0.75, pacY-1.5);
	}
	else if (pacman.getDir() == 3)
	{
		camPos = glm::vec3(pacX +1.5, 0.75, pacY);
	}

	// Camera matrix
	View = glm::lookAt (
		camPos,				// Camera is at camPos, in World Space
							// and looks at pacman
		glm::vec3(pacX, 0.5, pacY), 
		glm::vec3(0, 1, 0)  // Head is up (set to 0,-1,0 to look upside-down)
	);
}








// Resets starting positions, and other info
void Game::creatureRestart(int hs, int lf) 
{
	// Fill a temp with the right info
	PacMan tmpM('M', level, programID);
	Creature tmpB('B', level, programID);
	Creature tmpP('P', level, programID);
	Creature tmpI('I', level, programID);
	Creature tmpC('C', level, programID);

	// Fill the actual creatures with the temp
	pacman = tmpM;
	blinky = tmpB;
	pinky = tmpP;
	inky = tmpI;
	clyde = tmpC;
	safeGhostTimer = 0.0f;
}


// Restarts "Everything", or everything exept highscore
void Game::fullRestart(int hs, int lf) 
{
	level.resetPellets();
	level.countPellets();
	creatureRestart(hs, lf);
}








// Loads 2D models arrays
void::Game::load2DModels(char t[50], int maxI, int charPlace, Model_2D * model)
{
	// Creates changable path to send  through
	char tmpTXT[50];
	char * pathTXT;

	strcpy(tmpTXT, t);

	// Fills inn the numnbers Model array
	for (int i = 0; i < maxI; i++)
	{
		// Changes the char which has the number of texture to the right number
		tmpTXT[charPlace] = '0' + i;
		pathTXT = tmpTXT;

		// Creats a temp 2D model and adds it to the actual model
		Model_2D tmp(pathTXT, GUIProgramID);
		model[i] = tmp;
	}

	// Delete the pointer
	pathTXT = NULL;
	delete pathTXT;
}


// Sets same positions to all in Choice 2DModels array
void Game::m_sLoop2DModels(Model_2D * model, double y, double x)
{
	for (int i = 0; i < 2; i++)
	{
		model[i].make_square(glm::vec2(x, y), 1);
	}
}


// Draws HighScores
void Game::drawHS(int hs, double x, double y, double xMove, double scale)
{
	int nr = 0, div = 100000, mod = 1000000;
	// Getting each individual number in the Highscore
	// Starts by getting the first(biggest) number
	for (int i = 0; i < 6; i++) 
	{
		nr = hs;	// Set nr to actual higscore each time
		nr %= mod;	// Modulo away the numbers before
		nr /= div;	// Divide away the numbers after

					// Move the next number a step to the right
		numbers[nr].make_square(glm::vec2(x + (xMove*i), y), scale);
		numbers[nr].draw();

		div /= 10;	// Make div and mod 10 times smaller, to move one number back
		mod /= 10;
	}
}









// Fills in highscore list
void Game::initializeHS() 
{
	std::ifstream readHS("../res/level_data/hs.txt");
	if (readHS)			// If file exist, read in file
	{
		for (int i = 0; i < 10; i++)
		{
			readHS >> highscoreList[i];
			readHS.ignore();
		}
	}
	else				// Else fill inn with "empty" numbers
	{
		for (int i = 0; i < 10; i++)
		{
			highscoreList[i] = 1000000;
		}
	}

}

// Checks and fills in if new Highscore
bool Game::newHighscore() 
{
	for (int i = 0; i < 10; i++)	// Goes through the list
	{								// If pacman is higher then list[i]
		if (highscoreList[i] < pacman.compareScore())
		{
			if (i < 9)				// If i is bigger than 9
			{						//	move list a step down
				for (int j = 9; j > i; j--) 
				{
					highscoreList[j] = highscoreList[j - 1];
				}
			}						// Put new Highscore in list
			highscoreList[i] = pacman.compareScore();
			printHS();				// Prints out score each time it changes
			return false;			// Returns false to make sure it doesn't resave the highscore
		}
	}
} 

// Prints out file/Saves the game
void Game::printHS()
{
	// Writes out each score
	std::ofstream writeHS("../res/level_data/hs.txt");
	for (int i = 0; i < 10; i++)
	{
		writeHS << highscoreList[i] << "\n";
	}
}








// Collects all the light position and strenght, to send to other object to be able to refelect
std::vector<glm::vec3> Game::getLights()
{
	std::vector<glm::vec3> lights = level.getLights();
	lights.push_back(glm::vec3(blinky.getX(), 0.5, blinky.getY()));
	lights.push_back(glm::vec3(pinky.getX(), 0.5, pinky.getY()));
	lights.push_back(glm::vec3(inky.getX(), 0.5, inky.getY()));
	lights.push_back(glm::vec3(clyde.getX(), 0.5, clyde.getY()));

	return lights;
}

// Collects all the light colour, to send to other object to be able to refelect
std::vector<glm::vec3> Game::getLightColors()
{
	std::vector<glm::vec3> lighColor = level.getLightColor();
	lighColor.push_back(glm::vec3(1,1,1));
	lighColor.push_back(glm::vec3(1, 1, 1));
	lighColor.push_back(glm::vec3(1, 1, 1));
	lighColor.push_back(glm::vec3(1, 1, 1));

	return lighColor;
}
