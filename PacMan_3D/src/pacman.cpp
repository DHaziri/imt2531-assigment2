#include <pacman.h>
#include <iostream>
#include <math.h>
#include <string>


// hs needs to be 1000000 to properly convert int bits to char
//	Only the last 6 digit willbe shown and recorded
PacMan::PacMan(char c, Level l, GLuint PID, int p, int lf) : Creature (c, l, PID)
{ 
	// Gets inn how much points and life pacman has had from before, if any.
	hs = p; 
	life = lf; 
}


// Changes the amount of life
void PacMan::changeLife(int i, GLuint PID)
{ 
	if (life < 10)
	{
		life += i;
		if (life > 0)
		{
			changePacmanTexture(life, PID);
		}
	}
}
// Returns the amount of life Pacman has
int  PacMan::getLife() 
{ 
	return life; 
}

// Adds to the highscore
void PacMan::addHS(int s) 
{ 
	hs += s; 
}
// Returns score
int PacMan::compareScore()
{
	return hs;
}

/*char PacMan::getScore(int i) {

	int nr = 0, div = 100000, mod = 1000000;
	div /= pow(10, i);
	mod /= pow(10, i);
	nr = hs;
	nr %= mod;
	nr /= div;

	if (nr == 0) {return '0'; }
	else if (nr == 1) { return '1'; }
	else if (nr == 2) { return '2'; }
	else if (nr == 3) { return '3'; }
	else if (nr == 4) { return '4'; }
	else if (nr == 5) { return '5'; }
	else if (nr == 6) { return '6'; }
	else if (nr == 7) { return '7'; }
	else if (nr == 8) { return '8'; }
	else if (nr == 9) { return '9'; }
}*/





/*void PacMan::draw(const double dt, int xl, int yl, int dirl)
{
	if (xl == 0) { xl = x; }
	if (yl == 0) { yl = y; }
	if (dirl == 0) { dirl = dir; }

	/*if (ifCat) {
		drawBox(xl, yl, animStep, dirl+4);
		if(srtGame){
		for (int i = 0; i < life; i++) 
		{ drawBox(3+i, LVLH, 0, 6); }
		}
	}
	else {
		drawBox(xl, yl, animStep, dirl);
		if(srtGame){
		for (int i = 0; i < life; i++) 
		{ drawBox(3+i, LVLH, 0, 2); }
		}
	}


	if (drawTimer > 0) {
		drawTimer -= dt;
	}
	else {
		animStep++;
		if (animStep == 4)
			animStep = 0;

		 drawTimer = 0.06;
	}




}
*/

// Manages how many ghost pacman has eaten
void PacMan::hasEaten(bool ate, GLuint PID)
{
	// If ghost is eaten
	if (ate) {

		// Add to how many eaten, add to highscore
		amountEaten++;
		addHS(pow(200, amountEaten));

		// If all of them are eaten, reset counter add life
		if (amountEaten == 4)
		{
			amountEaten = 0;
			changeLife(1, PID);
		}
	}
	// If function is started but PacMan did not eat
	//	reset counter 
	else if (!ate) 
	{
		amountEaten = 0;
	}
}


// Stores users wish for direction next
void PacMan::changeNextDir(int i)
{
	nextDir = i;
}
// Changes the direction of the camrea and model
void PacMan::changeDir(Level l)
{
	int newDir = dir + nextDir;
	if (newDir < 0)
		newDir += 4;
	else if (newDir > 3)
		newDir -= 4;

	if (newDir == 0 && l.ifRoadBlock(y - 1, x) != '1' && l.ifRoadBlock(y - 1, x) != '2')
	{
		dir = newDir;
		nextDir = 0;
	}
	else if (newDir == 1 && l.ifRoadBlock(y, x + 1) != '1' && l.ifRoadBlock(y, x+1) != '2')
	{
		dir = newDir;
		nextDir = 0;
	}
	else if (newDir == 2 && l.ifRoadBlock(y+ 1, x) != '1' && l.ifRoadBlock(y + 1, x) != '2')
	{
		dir = newDir;
		nextDir = 0;
	}
	else if (newDir == 3 && l.ifRoadBlock(y, x-1) != '1' && l.ifRoadBlock(y, x-1) != '2')
	{
		dir = newDir;
		nextDir = 0;
	}
}
// Moves Pacman
void PacMan::move(const double dt, Level l, int cl)
{
	if (moveTimer > 0) 
	{
		moveTimer -= dt;
	}
	else 
	{
		changeDir(l);

		if (dir == 0 && l.ifRoadBlock(y-1, x) != '1' && l.ifRoadBlock(y - 1, x) != '2')
		{
			if (y == 0)
			{
				y = lvlH - 1;
			}
			else
			{
				y--;
			}
		}
		else if (dir == 1 && l.ifRoadBlock(y, x + 1) != '1' && l.ifRoadBlock(y , x +1) != '2')
		{
			if (x == lvlW - 1)
			{
				x = 0;
			}
			else
			{
				x++;
			}

		}
		else if (dir == 2 && l.ifRoadBlock(y+1, x) != '1' && l.ifRoadBlock(y + 1, x) != '2')
		{
			if (y == lvlH - 1)
			{
				y = 0;
			}
			else
			{
				y++;
			}
		}
		else if (dir == 3 && l.ifRoadBlock(y, x - 1) != '1' && l.ifRoadBlock(y , x-1) != '2')
		{
			if (x == 0)
			{
				x = lvlW - 1;
			}
			else
			{
				x--;
			}
		}

		moveTimer = BASEANIMOVETIMER - FASTERSPEED*cl;
	}
}



