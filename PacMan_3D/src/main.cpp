#include <graphics_functions.h>
#include <game.h>


Game game;			// Declares game class

					// Declares controls input function
void handleInput(GLFWwindow * window, int key, int scancode, int action, int mods);


int main()
{					// Starts controls
	glfwSetKeyCallback(game.window, handleInput);

	game.start();	// Stars game

	return 0;
}


// Controls input function
void handleInput(GLFWwindow * window, int key, int scancode, int action, int mods)
{					
	game.controls(window, key, scancode, action, mods);
}