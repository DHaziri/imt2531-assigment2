#include <level.h>
#include <global.h>


// Reads inn information form file
Level::Level(std::ifstream & rl, std::ifstream & rp, GLuint PID, GLuint GUIPID)
{
	// Reads in walls, coardinates, etc
	rl >> lvlH; rl >> lvlW; rl.ignore();
	roadBlocks.resize(lvlH);
	for (int i = 0; i < lvlH; i++) 
	{
		roadBlocks[i].resize(lvlW);
		for (int j = 0; j < lvlW; j++) 
		{	
			rl >> roadBlocks[i][j];
		}
		rl.ignore();
	}

	// Reads in pellets
	points.resize(lvlH);
	for (int i = 0; i < lvlH; i++) 
	{
		points[i].resize(lvlW);
		for (int j = 0; j < lvlW; j++) 
		{	
			rp >> points[i][j];	
		}
		rp.ignore();
	}



	// Creates temps of all the models on the level, to send in the right info
	Model_3D tmpB("../res/objects/Bush.obj", "../res/textures/HazelnutBark.png", PID);
	Model_3D tmpBP("../res/objects/Sunflower.obj", "../res/textures/Sunflower.jpg", PID);
	Model_3D tmpSP("../res/objects/Flower.obj", "../res/textures/Flower.jpg", PID);
	Model_3D tmpT("../res/objects/Apple_Tree.obj", "../res/textures/HazelnutBark.png", PID);
	Model_3D tmpA("../res/objects/Apple.obj", "../res/textures/Apple.jpg", PID);
	
	Model_2D tmpAT("../res/sprites/apple.png", GUIPID);

	// Then fills the actual models with the temps
	bush = tmpB;
	bigPellet = tmpBP;
	smallPellet = tmpSP;
	tree = tmpT;
	apple = tmpA;

	appleTracker = tmpAT;



	// Creates changable path to send  through
	char tmpTXT[50] = "../res/objects/water/WaterFrame0.obj";
	char * pathTXT;

	// Fills inn the pacman Model animation array
	for (int i = 0; i < ANIFRAMES; i++)
	{
		// Changes the char which has the number of model to the right number
		tmpTXT[31] = '0' + i;
		pathTXT = tmpTXT;

		// Creats a temp 3D model and adds it to the actual model
		Model_3D tmpW(pathTXT, "../res/textures/blue.jpg", PID);
		water[i] = tmpW;
		//creature.push_back(tmp);
	}

	// Delete the pointer
	pathTXT = NULL;
	delete pathTXT;



	// Generate the ground and sky
	buildGround(PID);
	buildSky(PID);
}



// Draws every RoadBlock, Pellet etc
void Level::draw(const double dt, int f, glm::mat4 proj, glm::mat4 view, std::vector<glm::vec3> lights, std::vector<glm::vec3> lightColors)
{
	// Draws/Generates ground and sky
	ground.draw(glm::vec3(0, 0, 0), proj, view, lights, lightColors);
	skyBox.draw(glm::vec3(0, 0, 0), proj, view, lights, lightColors);


	// Draws every pellet
	for (int i = 0; i < points.size(); i++)			// Y axis
	{
		for (int j = 0; j < points[i].size(); j++)	// X Axis
		{
			// Draws pellets, 1 for small(flower), 5 for big(sunflower), 3 for apple
			if (points[i][j] == '1')
			{
				smallPellet.draw(glm::vec3(j, 0, i), proj, view, lights, lightColors);
			}
			else if (points[i][j] == '3' && fruit)
			{
				apple.draw(glm::vec3(j, 0, i), proj, view, lights, lightColors);
			}
			else if (points[i][j] == '5')
			{
				bigPellet.draw(glm::vec3(j, 0, i), proj, view, lights, lightColors);
			}
		}
	}

	
	// Draws bushes/obsticals, 1 for bushes, 2 for water(6 is both water and spawn points)
	for (int i = 0; i < roadBlocks.size(); i++)			// Y axis
	{
		for (int j = 0; j < roadBlocks[i].size(); j++)	// X Axis
		{ 
																	
			if (roadBlocks[i][j] == '1')				// Draws bushes
			{	
				bush.draw(glm::vec3(j, 0, i), proj, view, lights, lightColors);
			}
			else if (roadBlocks[i][j] == '1' || roadBlocks[i][j] == '6')
			{
				water[f].draw(glm::vec3(j, 0, i), proj, view, lights, lightColors);
			}
		}
	}


	// Draws how many apples have been eaten
	int i = 0;
	while (fruitEaten != 0 && i < fruitEaten && gameMode == GAME)
	{	// Makes sure to move the drawing 0.04 step along the x axis, for every apple
		appleTracker.make_square(glm::vec2(0.86 - (0.04 *i++), -0.895), 0.02);
		appleTracker.draw();
	}
}



// Sends out information of a roadBlock or not
char Level::ifRoadBlock(int i, int j) 
{
	// 1 are walls, 2 are ghost aeria, o are open path
	if (i >= 0 && i < roadBlocks.size() && j >= 0 && j < roadBlocks[i].size())
	{
		return roadBlocks[i][j];
	}
	return '1';
}



// Gets the right nr to look for the right coordinates
char Level::findChar(char c) {
	if (c == 'M')		// Pacman starting position
	{ 
		return '4'; 
	}		
	else if (c == 'B')	// Blinkys starting position
	{ 
		return '5'; 
	}	
	else if (c == 'X')	// The other ghots starting position
	{ 
		return '6'; 
	}	
	return ' ';
}

// Gets X coordinates
int Level::getXC(char c) {
	int i = 0;
	char n = findChar(c);				// Gets nr to look for

	for (int i = 0; i < roadBlocks.size(); i++) {		 // Y axis
		for (int j = 0; j < roadBlocks[i].size(); j++) { // X axis
			if (roadBlocks[i][j] == n) { return j; }
		}
	}
}
// Gets Y coordinates
int Level::getYC(char c) {
	int i = 0;
	char n = findChar(c);				// Gets nr to look for

	for (int i = 0; i < roadBlocks.size(); i++) {		 // Y axis
		for (int j = 0; j < roadBlocks[i].size(); j++) { // X axis
			if (roadBlocks[i][j] == n) { return i; }
		}
	}
}

// Gets height
int Level::getHeight()
{
	return lvlH;
}

// Gets weight
int Level::getWidth()
{
	return lvlW;
}



// Sees if fruit can be spawned out or not
void Level::ifFruit(const double dt) 
{
	double i = 0;
	if (fruitTimer > 0) 
	{
		fruitTimer -= dt;
	}
	else {
		fruit = true;	// fruit Spawned in a radom time between certian restraints
		do 
		{ 
			i = rand(); 
		} while (i < 5 && i > 50);
		fruitTimer = i;
	}
}


// Checks if PacMan meets a pellet
int Level::checkPellets(int y, int x)
{	
	if (points[y][x] == '1')		// Meet small pellet
	{			
		points[y][x] = '2';			// Give a new value, instead of removing
		amountPellets--;			// One less pellet to eat	
		if (amountPellets == 0)		// If all pellets eaten = new level
		{
			gameReset = NEWLEVEL;
		}
		return 10;					// Add to highscore
	}
	else if (points[y][x] == '5')	// Meet big pellet
	{
		points[y][x] = '6';			// Give a new value, instead of removing
		amountPellets--;			// One less pellet to eat
		if (amountPellets == 0)		// If all pellets eaten = new level
		{
			gameReset = NEWLEVEL;
		}
		return 50;					// Add to highscore
	}
	else if (points[y][x] == '3' && fruit) 
	{
		fruitEaten++;				// Show how many fruits
		fruit = false;				// Remve fruit
		return 100;					// Add to highscore
	}

	return 0;						// If no points to return
}


// Counts how many pellets there are
void Level::countPellets() 
{
	for (int i = 0; i < lvlH; i++)		// y axis
	{
		for (int j = 0; j < lvlW; j++)	// x axis
		{
			if (points[i][j] == '1' || points[i][j] == '5')
			{ 
				amountPellets++; 
			}
		}
	}
}

int Level::howManyPellets() 
{
	return amountPellets;
}


// Reset the pellets
void Level::resetPellets() 
{
	for (int i = 0; i < lvlH; i++) 
	{
		for (int j = 0; j < lvlW; j++)
		{
			if (points[i][j] == '2')		// As we didn't remove the value,
			{								//	 we can easily reset 
				points[i][j] = '1'; 
			}			 
			else if (points[i][j] == '6')
			{ 
				points[i][j] = '5'; 
			}
		}
	}
	if(gameReset == FULL)	// After game over, remove fruit too
	{ 
		fruitEaten = 0; 
	} 
}


// Creates a randomized textured ground
void Level::buildGround(GLuint PID)
{
	int res = 2;
	unsigned short groundH = lvlH*res+ 50;
	unsigned short groundW = lvlW * res + 50;
	
	std::vector<unsigned short> indices;
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;

	
	for (int y = 0; y < groundH; y++)
	{
		for (int x = 0; x < groundW; x++)
		{
			

			//if lvlv[i][j] == '2'
			int j = (x - 30) / res;
			int i = (y - 30) / res;
			if(i >= 0 && i < lvlH && j >= 0 && j < lvlW && (roadBlocks[i][j] == '2' || roadBlocks[i][j] == '6'))
			{
				//float height = (((rand() % 10) - 5.0) / 100.0);
				vertices.push_back(glm::vec3((1.0 / res * x) - 0.5 - 15, -0.5, (1.0 / res * y) - 0.5 - 15));
				uvs.push_back(glm::vec2(x % 2, y % 2));
				normals.push_back(glm::vec3(0, 1, 0));
			}
			else
			{
				float height = (((rand() % 10) - 5.0) / 100.0);
				vertices.push_back(glm::vec3((1.0 / res * x) - 0.5 - 15, height, (1.0 / res * y) - 0.5 - 15));
				uvs.push_back(glm::vec2(x % 2, y % 2));
				normals.push_back(glm::vec3(0, 1, 0));
			}
		}
	}


	for (unsigned short i = 0; i < groundH -1; i++)
	{
		for (unsigned short j = 0; j < groundW -1; j++)
		{
			indices.push_back(i*groundW +j);
			indices.push_back(i*groundW + j + 1);
			indices.push_back(i*groundW + j + (groundW));

			indices.push_back(i*groundW + j + 1);
			indices.push_back(i*groundW + j + (groundW));
			indices.push_back(i*groundW + j + (groundW)+1);
		}
	}

	Model_3D tmpGround("../res/textures/peter_akimoto.dds", PID, vertices, uvs, normals, indices);
	ground = tmpGround;
}



void Level::buildSky(GLuint PID)
{
	std::vector<unsigned short> indices;
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;

	int cube = lvlH;
	if (lvlH < lvlW)
	{
		cube = lvlW;
	}
	cube += 2;



	// Sky
	vertices.push_back(glm::vec3(-cube, cube, cube));
	vertices.push_back(glm::vec3(-cube, cube, -cube));
	vertices.push_back(glm::vec3(cube, cube, cube));
	vertices.push_back(glm::vec3(cube, cube, -cube));

	uvs.push_back(glm::vec2(0.25, 1));
	uvs.push_back(glm::vec2(0.5, 1));
	uvs.push_back(glm::vec2(0.25, 0.66));
	uvs.push_back(glm::vec2(0.5, 0.66));

	normals.push_back(glm::vec3(0, -1, 0));
	normals.push_back(glm::vec3(0, -1, 0));
	normals.push_back(glm::vec3(0, -1, 0));
	normals.push_back(glm::vec3(0, -1, 0));

	// West
	vertices.push_back(glm::vec3(-cube, cube, -cube));
	vertices.push_back(glm::vec3(cube, cube, -cube));
	vertices.push_back(glm::vec3(-cube, -cube, -cube));
	vertices.push_back(glm::vec3(cube, -cube, -cube));

	uvs.push_back(glm::vec2(0, 0.66));
	uvs.push_back(glm::vec2(0.25, 0.66));
	uvs.push_back(glm::vec2(0, 0.33));
	uvs.push_back(glm::vec2(0.25, 0.33));

	normals.push_back(glm::vec3(0, 0, 1));
	normals.push_back(glm::vec3(0, 0, 1));
	normals.push_back(glm::vec3(0, 0, 1));
	normals.push_back(glm::vec3(0, 0, 1));

	// North
	vertices.push_back(glm::vec3(cube, cube, cube));
	vertices.push_back(glm::vec3(cube, cube, -cube));
	vertices.push_back(glm::vec3(cube, -cube, -cube));
	vertices.push_back(glm::vec3(cube, -cube, cube));

	uvs.push_back(glm::vec2(0.25, 0.66));
	uvs.push_back(glm::vec2(0.5, 0.66));
	uvs.push_back(glm::vec2(0.25, 0.33));
	uvs.push_back(glm::vec2(0.5, 0.33));

	normals.push_back(glm::vec3(-1, 0, 0));
	normals.push_back(glm::vec3(-1, 0, 0));
	normals.push_back(glm::vec3(-1, 0, 0));
	normals.push_back(glm::vec3(-1, 0, 0));

	// East
	vertices.push_back(glm::vec3(cube, cube, cube));
	vertices.push_back(glm::vec3(-cube, cube, cube));
	vertices.push_back(glm::vec3(cube, -cube, cube));
	vertices.push_back(glm::vec3(-cube, -cube, cube));

	uvs.push_back(glm::vec2(0.5, 0.66));
	uvs.push_back(glm::vec2(0.75, 0.66));
	uvs.push_back(glm::vec2(0.5, 0.33));
	uvs.push_back(glm::vec2(0.75, 0.33));

	normals.push_back(glm::vec3(0, 0, -1));
	normals.push_back(glm::vec3(0, 0, -1));
	normals.push_back(glm::vec3(0, 0, -1));
	normals.push_back(glm::vec3(0, 0, -1));

	// South
	vertices.push_back(glm::vec3(-cube, cube, cube));
	vertices.push_back(glm::vec3(-cube, cube, -cube));
	vertices.push_back(glm::vec3(-cube, -cube, cube));
	vertices.push_back(glm::vec3(-cube, -cube, -cube));

	uvs.push_back(glm::vec2(0.75, 0.66));
	uvs.push_back(glm::vec2(1, 0.66));
	uvs.push_back(glm::vec2(0.75, 0.33));
	uvs.push_back(glm::vec2(1, 0.33));

	normals.push_back(glm::vec3(1, 0, 0));
	normals.push_back(glm::vec3(1, 0, 0));
	normals.push_back(glm::vec3(1, 0, 0));
	normals.push_back(glm::vec3(1, 0, 0));
	
	// Ground
	vertices.push_back(glm::vec3(cube, -cube, -cube));
	vertices.push_back(glm::vec3(cube, -cube, cube));
	vertices.push_back(glm::vec3(-cube, -cube, -cube));
	vertices.push_back(glm::vec3(-cube, -cube, cube));

	uvs.push_back(glm::vec2(0.25, 0.33));
	uvs.push_back(glm::vec2(0.5, 0.33));
	uvs.push_back(glm::vec2(0.25, 0));
	uvs.push_back(glm::vec2(0.5, 0));



	normals.push_back(glm::vec3(0, 1, 0));
	normals.push_back(glm::vec3(0, 1, 0));
	normals.push_back(glm::vec3(0, 1, 0));
	normals.push_back(glm::vec3(0, 1, 0));

	
	for (int i = 0; i < 6; i++)			// For each square
	{
		for (int j = 0; j < 2; j++)		// For each triangle in each sqaure
		{
			for (int k = 0; k < 3; k++) // For each vertex in each triangle
			{
				indices.push_back(i * 4 + j + k);
			}
		}
	}

	Model_3D tmpSky("../res/textures/Sky.png", PID, vertices, uvs, normals, indices);
	skyBox = tmpSky;

}

std::vector<glm::vec3> Level::getLights()
{
	std::vector<glm::vec3> lights;
	for (int i = 0; i < points.size(); i++)
	{
		for (int j = 0; j < points[i].size(); j++)
		{
			if (points[i][j] == '5')
			{
				lights.push_back(glm::vec3(j, 0.5, i));
			}
			if (points[i][j] == '3' && fruit)
			{
				lights.push_back(glm::vec3(j, 0.25, i));
			}
		}
	}
	return lights;
}

std::vector<glm::vec3> Level::getLightColor()
{
	std::vector<glm::vec3> lights;
	for (int i = 0; i < points.size(); i++)
	{
		for (int j = 0; j < points[i].size(); j++)
		{
			if (points[i][j] == '5')
			{
				lights.push_back(glm::vec3(0.9, 0.9, 0));
			}
			if (points[i][j] == '3' && fruit)
			{
				lights.push_back(glm::vec3(0.9, 0,0 ));
			}
		}
	}
	return lights;
}