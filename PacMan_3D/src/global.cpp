#include <global.h>


// Structs
GameMode gameMode = MAINMENU;			// Enum to check which game mode
GameReset gameReset = NORESET;			// Enum to check what type of game reset to execute, if any


// Consts
static const int TXTLEN = 50;			// Lenght of text
const int ANIFRAMES = 6;				// Frames of animation
const double MAXGHOSTTIME = 50.0;		// How long ghosts can be edible
const double BASEANIMOVETIMER = 0.06;	// Base nr all animation and movement timers should start on
const double FASTERSPEED = 0.01;		// How much movement speed should change after each completed levels

bool dayLight = false;