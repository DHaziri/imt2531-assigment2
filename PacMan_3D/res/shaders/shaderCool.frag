#version 330 core

//
// Shader for things that need light (3D)
//


// Interpolated values from the vertex shaders
in vec2 UV;
in vec3 Position_worldspace;
in vec3 Normal_cameraspace;
in vec3 EyeDirection_cameraspace;
in vec3 LightDirection_cameraspace[9];

// Ouput data
out vec4 color;

// Values that stay constant for the whole mesh.
uniform float ambientPower;
uniform sampler2D myTextureSampler;
uniform vec3 LightPosition_worldspace[9];	// Lights position on the level
uniform vec3 LightColor[9];					// The colour of the different lights
uniform bool LightOn[9];					// If the lights are in use at all

void main() 
{
	float LightPower = 0.5f;

	// If alpha is too small, dont draw
	if(texture( myTextureSampler, UV ).a < 0.01)
	{
		discard;
	}
	
	// Material properties
	vec3 MaterialDiffuseColor = texture( myTextureSampler, UV ).rgb;
	vec3 MaterialAmbientColor = vec3(0.1,0.1,0.1) * MaterialDiffuseColor;
	vec3 MaterialSpecularColor = vec3(0.3,0.3,0.3);

	// The final light shown
	vec3 totalLight = MaterialAmbientColor * ambientPower;


	for(int i = 0; i < 9; i++)
	{
		if(LightOn[i])
		{
			// Distance to the light
			float distance = length( LightPosition_worldspace[i] - Position_worldspace );

			// Normal of the computed fragment, in camera space
			vec3 n = normalize( Normal_cameraspace );
			// Direction of the light (from the fragment to the light)
			vec3 l = normalize( LightDirection_cameraspace[i] );
			// Cosine of the angle between the normal and the light direction, 
			// clamped above 0
			//  - light is at the vertical of the triangle -> 1
			//  - light is perpendicular to the triangle -> 0
			//  - light is behind the triangle -> 0
			float cosTheta = clamp( dot( n,l ), 0,1 );
	
			// Eye vector (towards the camera)
			vec3 E = normalize(EyeDirection_cameraspace);
			// Direction in which the triangle reflects the light
			vec3 R = reflect(-l,n);
			// Cosine of the angle between the Eye vector and the Reflect vector,
			// clamped to 0
			//  - Looking into the reflection -> 1
			//  - Looking elsewhere -> < 1
			float cosAlpha = clamp( dot( E,R ), 0,1 );

			totalLight += 
				// Diffuse : "color" of the object
				MaterialDiffuseColor * LightColor[i] * LightPower * cosTheta / (distance*distance) +
				// Specular : reflective highlight, like a mirror
				MaterialSpecularColor * LightColor[i] * LightPower * pow(cosAlpha,5) / (distance*distance);
		}
	}

	color = vec4(totalLight, texture( myTextureSampler, UV ).a);
}