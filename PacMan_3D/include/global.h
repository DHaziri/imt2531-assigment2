#pragma once

#include <enum.h>



// Structs
extern GameMode gameMode;				// Enum to check which game mode
extern GameReset gameReset;				// Enum to check what type of game reset to execute, if any


// Consts
extern const int TXTLEN;				// Lenght of text
extern const int ANIFRAMES;				// Frames of animation
extern const double MAXGHOSTTIME;		// How long ghosts can be edible
extern const double BASEANIMOVETIMER;	// Base nr all animation and movement timers should start on
extern const double FASTERSPEED;		// How much movement speed should change after each completed levels

extern bool dayLight;
