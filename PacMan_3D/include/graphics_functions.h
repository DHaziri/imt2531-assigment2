#pragma once

#include <GL/glew.h>
#include <vector>
#include <glm/glm.hpp>

namespace Graphics
{
    GLuint loadShaders(char const* vertPath, char const* fragPath);

    GLuint loadTexture(char const* path);

    bool loadAssImp(const char* path, std::vector<unsigned short>& indices, std::vector<glm::vec3>& vertices, std::vector<glm::vec2>& uvs, std::vector<glm::vec3>& normals);
}