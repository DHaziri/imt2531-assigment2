#pragma once
#include <creature.h>
#include <level.h>


class PacMan : public Creature {
private:
	// Variables
	int hs;					// Highscore
	int life;				// How much life
	int amountEaten = 0;	// Tracker on how many ghost Pacman has eate
	int nextDir;			// Stores users wish for direction next

	// Function
	void changeDir(Level l);// Changes the direction of the camrea and model

public:
							// hs needs to be 1000000 to properly convert int bits to char
							//	Only the last 6 digit willbe shown and recorded
	PacMan(char c, Level l, GLuint PID, int p = 1000000, int lf = 5) ;  											
	PacMan() {}	

	// Functions
							// Changes the amount of life
	void changeLife(int i, GLuint PID);	
	int  getLife();			// Returns the amount of life Pacman has

	void addHS(int s);		// Adds to the highscore
	int compareScore();		// Returns score
	//char getScore(int i);

	void hasEaten(bool ate, GLuint PID);// Manages how many ghost pacman has eaten 

	void changeNextDir(int i);					// Stores users wish for direction next
	void move(const double dt, Level l, int cl);// Moves Pacman
};
