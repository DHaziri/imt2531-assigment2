#pragma once
#include <level.h>
#include <global.h>
#include <string>

class Creature {
private:
	char creID;
	int ifOut = 0;
	bool eateble = false;
	bool eaten = false;
	double noKillTimer = 2;
	
	static const int MAXLIFE = 10;

	//char pmTexturePath[6][MAXLIFE];
	std::vector<std::string> pmTexturePath;

	//Model_3D creature;
	//Model_3D pacman[6];

	std::vector<Model_3D> creature;
	

protected:
	int y, x;
	int lvlH, lvlW;

	int animStep = 0;
	int dir = 0;

	double moveTimer = BASEANIMOVETIMER;
	double drawTimer = BASEANIMOVETIMER;

	void changePacmanTexture(int l, GLuint PID);


public:
	Creature(char c, Level l, GLuint PID);
	Creature() {}

	int virtual getDir();
	int virtual getY();
	int virtual getX();
	char getChar();
	void changeChar(char c);

	bool ifPacman(int yp, int xp);

	void makeEateble(bool e, bool t =  false);
	bool isEateble();
	bool noKill();

	void virtual draw(const double dt, int f, glm::mat4 proj, glm::mat4 view, std::vector<glm::vec3> lights, std::vector<glm::vec3> lightColor);
	void move(const double dt, Level level, int cl);
};