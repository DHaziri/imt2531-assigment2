#pragma once

#include <GL/glew.h>
#include <vector>

// Include GLM
#include <glm/glm.hpp>

class Model_3D
{
private:
	//
    std::vector<unsigned short> indices;
    std::vector<glm::vec3> indexed_vertices;
    std::vector<glm::vec2> indexed_uvs;
    std::vector<glm::vec3> indexed_normals;


	// Buffers
    GLuint vertexArrayID;
    GLuint vertexbuffer;
    GLuint uvbuffer;
    GLuint normalbuffer;
    GLuint elementbuffer;

	GLuint ProgramID;
	GLuint TextureID;

	// Uniforms
    GLuint MatrixID;		// 
	GLuint ViewMatrixID;
	GLuint ModelMatrixID;
	GLuint LightID;			//
	GLuint LightColorID;
	GLuint LightOnID;
	GLuint AmbientPowerID;
	

    GLuint texture;
    float rotation = 0;


    float scale = 0.5f;

public:
    Model_3D();
    Model_3D(char const* objPath, char const* texPath, GLuint PID);
	Model_3D(char const* texPath, GLuint PID, std::vector<glm::vec3> verts, std::vector<glm::vec2> uvs, std::vector<glm::vec3> normals, std::vector<unsigned short> i);

	void setBufferData();

    void draw(glm::vec3 pos, glm::mat4 proj, glm::mat4 view, std::vector<glm::vec3> lights, std::vector<glm::vec3> lightColor);
	void rescale(float s);
	void setRotation(float r);
	void updateModel(std::vector<glm::vec3> verts, std::vector<glm::vec2> uvs, std::vector<glm::vec3> normals, std::vector<unsigned short> i);
};