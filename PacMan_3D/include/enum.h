#pragma once

#include <glm/glm.hpp>
#include <GLFW/glfw3.h>

enum GameReset { FULL, NEWLEVEL, LOSTLIFE, NORESET };
enum GameMode { MAINMENU, GAME, PAUSE, GAMEOVER, HS, INFO };


struct Highscore 
{
	int nrScore;
};


// Defines a renderable object used in this example
struct Renderable
{
	GLuint vao{};
	GLuint vbo{};
	GLuint ebo{};
	GLuint index_count{};
};

// Defines the vertex layout used for this example
//  Also - Task 3 is related to this Vertex
struct Vertex
{
	glm::vec2 position;
	glm::vec3 color;
	glm::vec2 texcoord;
};


