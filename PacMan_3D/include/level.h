#pragma once

#include <vector>
#include <fstream>
#include <iostream>
#include <model_3D.h>
#include <model_2D.h>
#include <GL/glew.h>


class Level {
private:
	std::vector<std::vector<char>> roadBlocks;
	std::vector<std::vector<char>> points;
	int lvlH, lvlW;			// lvlH = Height(Lenght) og the level, lvlW = Width og the level


	// Imported 3D models
	Model_3D bush;
	Model_3D bigPellet;
	Model_3D smallPellet;
	Model_3D tree;
	Model_3D apple;
	Model_3D water[6];

	// Generated 3D models
	Model_3D ground;
	Model_3D skyBox;

	Model_3D testWater;


	// 2D Models
	Model_2D appleTracker;


	// Variables
	int waterAni = 0;

	double waterTimer = 0.5;


	int amountPellets = 0;	// Trackers
	int fruitEaten = 0;

	bool fruit = false;		
							
	double fruitTimer = 20.0;// Timers
	int blinkTimer = 0.5;

public:
	Level() {}				// Needed to create global level
							// Reads inn information form file
	Level(std::ifstream & rl, std::ifstream & rp, GLuint PID, GLuint GUIPID);

	void draw(const double dt, int f, glm::mat4 proj, glm::mat4 view, std::vector<glm::vec3> lights, std::vector<glm::vec3> lightColors);	// Draws every RoadBlock, Pellet etc
							// Sends out information of a roadBlock or not
	char ifRoadBlock(int i, int j);

					
	char findChar(char c);	// Gets the right nr to look for the right coordinates
	int getXC(char c);		// Gets X coordinates
	int getYC(char c);		// Gets Y coordinates

	int getHeight();		// Gets height
	int getWidth();			// Gets weight

							// Sees if fruit can be spawned out or not
	void ifFruit(const double dt);

	int checkPellets(int x, int y);	// Checks if PacMan meets a pellet
	void countPellets();			// Counts how many pellets there are
	int howManyPellets();			// Returns how many pellets there are
	void resetPellets();			// Reset the pellets

	void buildGround(GLuint PID);	// Creates a randomized textured ground
	void buildSky(GLuint PID);		// Creates the skybox
	std::vector<glm::vec3> getLights();
	std::vector<glm::vec3> getLightColor();
};