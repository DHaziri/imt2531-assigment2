#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <model_3D.h>
#include <pacman.h>

//
class Game
{
private:

    GLuint programID;
	GLuint GUIProgramID;

    glm::mat4 Projection;	// Projection matrix
    glm::mat4 View;			// Camera matrix

	// Classes
	Level level;

	PacMan pacman;
	Creature blinky;
	Creature pinky;
	Creature inky;
	Creature clyde;


	// 2D Models
	Model_2D numbers[10];	// Model of the numbers to draw HS
	Model_2D higscoreBox;	// Model of bakground for pause
	Model_2D menuBox;		// Model of background for highscore 

	Model_2D tPacMan;			// Titles
	Model_2D tHighscores;
	Model_2D tPause;
	Model_2D tGameOver;
	Model_2D tTryAgain;

	Model_2D cStartGame[2];		// Choices
	Model_2D cHighscores[2];	//	0 is the non selected, 1 is selected
	Model_2D cQuit[2];
	Model_2D cResume[2];
	Model_2D cYes[2];
	Model_2D cNo[2];


	// Variables
	int highscoreList[10];	// Lists of Highscores
	int aniFrames = 0;		// Keeps track of the animation frames
	int optionSelected = 0;	// Keeps track of which option user is on
	int completedLevels = 0;// How many levels are completed
							// Times how long ghosts have
	double safeGhostTimer = MAXGHOSTTIME;	 
	bool saveHS = false;	// Checks if player wants to save HS


	// Functions
							// Loads 2D models arrays
	void load2DModels(char t[50], int maxI, int charPlace, Model_2D * model);
							// Sets same positions to  all in Choice 2DModels array
	void m_sLoop2DModels(Model_2D * model, double y, double x = 0.0);		


public:

	GLFWwindow* window; // Game Window

	// Functions
    Game();

    void start();					// The main game, acts as a new int main()
									// Handles the controls of the game
	void controls(GLFWwindow * window, int key, int scancode, int action, int mods);
	void mechanics(const double dt);// Handels the mechanics of the game
	void drawGame(const double dt);	// Draws the grafics
	void updateCamera(float dt);			// Updates camera in relation to Pacman

	void creatureRestart(int hs = 1000000, int lf = 5);	// Resets starting positions, and other info
	void fullRestart(int hs = 1000000, int lf = 5);		// Restarts "Everything", or everything exept highscore

									// Draws HighScores
	void drawHS(int hs, double x, double y, double xMove, double scale); 


	void initializeHS();			// Fills in highscore list
	bool newHighscore();			// Checks and fills in if new Highscore
	void printHS();					// Prints out file/Saves the game


	// Collects all the light position and strenght, to send to other object to be able to refelect
	std::vector<glm::vec3> getLights();
	// Collects all the light colour, to send to other object to be able to refelect
	std::vector<glm::vec3> getLightColors();
};