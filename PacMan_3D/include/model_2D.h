#pragma once

#include <GL/glew.h>
#include <vector>

// Include GLM
#include <glm/glm.hpp>

class Model_2D
{
private:
    std::vector<unsigned short> indices;		// Cointains the index to all the vertices
    std::vector<glm::vec2> indexed_vertices;	// Each vertex
    std::vector<glm::vec2> indexed_uvs;			// Vertices on the sprite

    GLuint vertexArrayID;		
    GLuint vertexbuffer;
    GLuint uvbuffer;
    GLuint elementbuffer;
	GLuint ProgramID;
	
	GLuint TextureID;
	
    GLuint texture;


public:
    Model_2D();
    Model_2D(char const* texPath, GLuint ProgramID);

	// To place the 2D sprites on the screen
	void make_square(glm::vec2 pos, float scale);

	void setBufferData();

    void draw();
};