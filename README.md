# imt2531_assignment2


Donjetë Haziri

# indroduction

I know I was supposed to be in a group, but because of the situation and personal reasons I became quite unreliable. At a certian point I even believed I wouldn't be able to deliver antything. So to not burden anyone by being a deadbeat, I didnt try to join anyone.
The theme is the overused theme "horror". 
It's in third person, and you play as a ladybug, in a outside maze at night.

##How to start the file
This guide assumes that you have both cMake and Visual Studio.

- Open cMake.
- Write "path/Pacman_3D" into "Where is the source code" and "path/Pacman_3D/build" into "Where to build the binaries"
- "Configurate", and build build
- "Generate"
- "Open project"
- Left click "ALL_BUILD" and select "Build"
- Left click "Solution 'pacman' ", and select "Properties"
- Change "Single startup project" from "ALL_BUILD" to "pacman"



## Controls

W - Scroll up/Go forward
A - Scroll left/ Go left
S - Scroll down/Go backwards
D - Scroll right/Go right
Enter - Choose/Select
P - Pause
Backspace - Go back/Pause

## 

- Main Menu
You can choose between to Start the game, to see Highscores or to quit the program

- Start Game
The actual game. It works exactly as Pacman does. If you go one direction, you will continue to go one direction until theres an obstacle. Small pellets(purple flowers) give 10 points. Big pellets (Sunflowers) give 50 points while also making the ghosts(jellyfish) eatable. The world lights up when it is Pacman(ladybug) who eats, and theres no fear of being eaten. Points is 100*2^the amount of ghosts eaten within the time frame. If you eat all, you gain a life. Fruit(apple) is 100 points, and spawns at somewhat random random times. If all pellets are eaten, the level will reset and both pacman and the ghosts will move faster.

The amount of fruits eaten is still drawn on screen, just underneath the highscore, but you see pacmans(ladybug) lifes on the back. Pacman can have a maximun of 10 lives.

If you press pause(P/Backspace), you can choose between resuming, or quiting. Quiting will make you lose all process.

If you lose all lives, games over, and if your high score is bigger then the 10 before it will be saved in the higscore tab. You can choose to try again or quit.

- Highscore
You can see all the highest scores gotten.
You can only go back.


## Ideas and choices

Changeable bool as global variables. 
It felt like it was one parameter too many. Feelings aren't exactly the best reason not to do something in programming, but as it only affected graphics, I decided it wouldn't be a big glitch if anything happened. Yes, if daylight turns on when it's not supposed to its not as immersing, and maybe a little confusing, but the gameplay itself is not in any way touched
Map
I was planning on making a map, even though I was not in first person. But as you still play so close to the ground, some type of map would have helped. The map was only gonna show pellets, as the map on one of those videos you showed us. Not much(fitting to the horror theme), but still better than endlessly looking for the last pellet. I didn't get the time to make it, tho.
Lagg.
To many vertices. I originally had a better looking, more high-end bush made from the hazelnut tree in the sources. It lagged even more, so I made my own bush. The small pellets also has alot of vertices that brings the lag, but the lag is not the worst, so making my own flower would have been at the bottom of my list
Flower light
No riggig, jellyfish animated


##sources

https://free3d.com/3d-model/apple-609206.html      Apple
https://free3d.com/3d-model/crocus-flower-v1--75899.html  -c

https://free3d.com/3d-model/hazelnut-bush-290678.html

https://free3d.com/3d-model/-sunflower-v1--572329.html

